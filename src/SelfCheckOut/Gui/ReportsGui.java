package SelfCheckOut.Gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.Timer;
import com.sun.xml.internal.ws.api.server.Container;
import java.awt.event.ActionListener;
import java.util.*;
import SelfCheckOut.Devices.BaggingArea;
import SelfCheckOut.Devices.BaggingAreaEvent;
import SelfCheckOut.Devices.BaggingAreaListener;
import SelfCheckOut.Devices.PaymentCollector;
import SelfCheckOut.Exceptions.AddWhileBaggingException;
import SelfCheckOut.Exceptions.AddWhilePayingException;
import SelfCheckOut.Exceptions.IncorrectStateException;
import SelfCheckOut.Exceptions.InvalidEmployeeNumberException;
import SelfCheckOut.Exceptions.InvalidProductException;

import SelfCheckOut.App.CategoryDB;
import SelfCheckOut.App.GroceryTableElement;
import SelfCheckOut.App.Reporter;

/**
* This class contains the main method that will show the Graphical User
* Interface of displaying reports.
*
*/
class ReportsGUI extends JPanel implements ActionListener{

/**
* Button for sorting Product Name
*/
protected JButton sortProduct;

/**
* Button for sorting Number of Purchases
*/
protected JButton sortNumPurchase;

/**
* Button for sorting Total Price 
*/
protected JButton sortTotalPrice;

/**
* Button for sorting Total Tax
*/
protected JButton sortTotalTax;

/**
* Button for sorting Category 
*/
protected JButton sortCategory;

/**
* Button for sorting Promotion
*/
protected JButton sortPromotion;

/**
* Text Area for the reports
*/	
protected JTextPane reportsPane;

/**
* Label for startDate
*/	
protected JButton startDateButton;

/**
* Label for endDate
*/	
protected JButton endDateButton; 

/**
* Text Field for category
*/	
protected JComboBox categoryBox;

/*
* The reporter class, used for retrieving
* all the reports
*/
protected Reporter reporter;


/**
* Initialize the ReportsGui by setting up all appropriate labels,
 * fields and buttons, positioning them and connecting up the action
 * listeners.
*/    
public ReportsGUI() {
    setLayout(new BorderLayout());
  
    // initialize the reporter class

    reporter = new Reporter();

    // start & end dates
    startDateButton = createDateButton();
    endDateButton = createDateButton();

    //Category ComboBox
    String[] catList = CategoryDB.getInstance().getCategories();
    categoryBox = new JComboBox(catList);
    categoryBox.setActionCommand("category");
    categoryBox.addActionListener(this);

	// title buttons

	sortProduct = constructButton("product name");
	sortNumPurchase = constructButton("Number of Purchases");
	sortTotalPrice = constructButton("Total Price");
	sortTotalTax = constructButton("Total Tax");
	sortCategory = constructButton("Category");
	sortPromotion = constructButton("Promotion");

    JPanel textControlsPane = alignElements();

    reportsPane = new JTextPane();

    // that's one shitty font, change it
    reportsPane.setFont(new Font("monospaced", Font.BOLD, 18));
    //reportsPane.setLineWrap(true);
    //sreportsPane.setWrapStyleWord(true);
    reportsPane.setContentType("text/html");
    reportsPane.setEditable(false);

    //Add scroll to the text area
    JScrollPane areaScrollPane = new JScrollPane(reportsPane);
    areaScrollPane.setVerticalScrollBarPolicy(
              JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    areaScrollPane.setPreferredSize(new Dimension(900, 400));

    //Include all the controls in the application panel
    JPanel appPanel = new JPanel(new BorderLayout());
    appPanel.add(textControlsPane, BorderLayout.PAGE_START);
    appPanel.add(areaScrollPane, BorderLayout.CENTER);

    add(appPanel, BorderLayout.LINE_START);
}

/* 
 * Initialize some decorative elements never accessed by the user:
 * the labels whose values do not change: "from", "until", and "from 
 * category"
 * It then positions all elements present in the ReportsGUI and aligns
 * them according to a grid
 */
private JPanel alignElements() {
// create non-modified labels
    JLabel startLabel = new JLabel("From");
    JLabel endLabel = new JLabel("until");
    
    //Category text label
    JLabel categoryLabel = new JLabel(" from category: ");
    categoryLabel.setLabelFor(categoryBox);

	//Lay out the text controls and the labels
    JPanel textControlsPane = new JPanel();
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();

    textControlsPane.setLayout(gridbag);
    c.anchor = GridBagConstraints.WEST;

    //Line 1: Specify search criteria

    positionElement(0, 0, c);
    textControlsPane.add(startLabel, c);

    positionElement(1, 0, c);
    textControlsPane.add(startDateButton, c);

    positionElement(2, 0, c);
    textControlsPane.add(endLabel, c);

    positionElement(3, 0, c);
    textControlsPane.add(endDateButton, c);

    positionElement(4, 0, c);
    textControlsPane.add(categoryLabel, c);

    positionElement(5, 0, c);
    textControlsPane.add(categoryBox, c);

    //Line 3: Sort buttons

    positionElement(0, 2, c);
    textControlsPane.add(sortProduct, c);

    positionElement(1, 2, c);
    textControlsPane.add(sortNumPurchase, c);

    positionElement(2, 2, c);
    textControlsPane.add(sortTotalPrice, c);

    positionElement(3, 2, c);
    textControlsPane.add(sortTotalTax, c);

    positionElement(4, 2, c);
    textControlsPane.add(sortCategory, c);

    positionElement(5, 2, c);
    textControlsPane.add(sortPromotion, c);

    return textControlsPane;
    }

/* Return a button which will have the today's date and
 * summon the Calendar on click. It is always initialized
 * with today's date.
 */
private JButton createDateButton() {
    Calendar now = Calendar.getInstance();
	int year = now.get(Calendar.YEAR);
    int month = now.get(Calendar.MONTH);
    int date = now.get(Calendar.DATE);
    String text = date + "/" + (month + 1) + "/" + year;

	JButton dateButton = new JButton(text);
    dateButton.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent evt) {
    		startDateCalendar(evt);
    	}
    });
    return dateButton;
}
/*
 * Given the x and y positions and a GridBagConstraints variable
 *, specify the position according to the input parameters.
 */
private void positionElement(int x, int y, GridBagConstraints con) {
	con.fill = GridBagConstraints.HORIZONTAL;
    con.weightx = 0;
    con.gridx = x;
    con.gridy = y;
}

/*
 * Create a new window with a Calendar. On close, update the reports
 * output in the reports area.
 */
private void startDateCalendar(ActionEvent evt) {
	JFrame f = new JFrame("Cal");
	java.awt.Container c = f.getContentPane();
	c.setLayout(new FlowLayout());
	Cal calendar = new Cal();
	calendar.setCallButton((JButton)evt.getSource());
	c.add(calendar);
	f.pack();
	f.setVisible(true);
	
	f.addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
			 Vector<GroceryTableElement> stuff = 
				reporter.getGroceryInfo(startDateButton.getText(), 
						 endDateButton.getText());
			 updateOutput(stuff);
		}
	});
}

/*
 * Return a button with the given name and the given action command.
 */
private JButton constructButton(String name) {
	JButton button = new JButton(name);
    button.setVerticalTextPosition(AbstractButton.BOTTOM);
    button.setHorizontalTextPosition(AbstractButton.CENTER);
    button.setActionCommand(name);
    button.addActionListener(this);
    return button;
}

/* 
 * Print the report to the reportsPane
 */
private void updateOutput(Vector<GroceryTableElement> tableList) {
    String start = "<html><body><table>";
    String content = "";
    for (GroceryTableElement element : tableList) {
        content += "<tr>";
        content += generateCell(element.getProductName());
        content += generateCell(element.getProductCategory());
        content += generateCell(element.printNumPurchases());
        content += generateCell(element.printTotalPrice());
        content += generateCell(element.printTotalTax());
        content += generateCell(element.printWeight());
        content += generateCell(element.printPromotion());
        content += "</tr>";
    }

    String end  = "</table></body></html>";
    reportsPane.setText(start + content + end);
}

private String generateCell(String property) {
    String cell = "<td width = 100px>" + property + "</td>";
    return cell;
}


/**
* Method that receives the ActionEvent when a button is pressed in the 
* GUI. It calls to the appropriate action in the system and
* shows the result of the action in the message text area.
* If an exception is raised, this is showed in the message text area
* starting with the word EXCEPTION.
* @param e ActionEvent captured when user presses a button in the GUI
*/       
public void actionPerformed(ActionEvent e) {
	//new ReportsGUI();
	//Instantiate actions class
    Actions actions = new Actions();

}
}