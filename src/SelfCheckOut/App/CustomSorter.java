/**
 * 
 */
package SelfCheckOut.App;

import java.util.Comparator;
import java.util.Vector;

/**
 * @author Zafer Sawaf
 * This class is responsible for sorting according to user request.
 * A user can choose to sort by Product name, Category, Number of Items
 * and Price.
 * 
 * I can't use comparators for this class due to the odd types, therefore
 * I'm going to do less efficient sorting.
 */
public class CustomSorter {
	
	/**
	 * Sort items by Product name. 
	 * Input is a vector of all the grocerytableelement,
	 */
	public Vector<GroceryTableElement> sortByProducts(Vector<GroceryTableElement> groc) {
		Vector<GroceryTableElement> sorted = new Vector<GroceryTableElement>();
		GroceryTableElement lowest;
		while (!groc.isEmpty()){
			lowest = getLowestString(groc, 0);
			sorted.add(lowest);
			groc.remove(lowest);
		}
		return sorted;
	}
	
	/**
	 * Sort items by Category
	 * @param A vector of all GroceryTableElements
	 * @return A sorted Vector
	 */
	
	public Vector<GroceryTableElement> sortByCategory(Vector<GroceryTableElement> groc) {
		Vector<GroceryTableElement> sorted = new Vector<GroceryTableElement>();
		GroceryTableElement lowest;
		while (!groc.isEmpty()){
			lowest = getLowestString(groc, 1);
			sorted.add(lowest);
			groc.remove(lowest);
		}
		return sorted;
	}
	
	/**
	 * Sort items by number of purchases.
	 * @param A vector of all GroceryTableElements
	 * @return A sorted Vector
	 */
	public Vector<GroceryTableElement> sortByPurchases(Vector<GroceryTableElement> groc, int option) {
		Vector<GroceryTableElement> sorted = new Vector<GroceryTableElement>();
		GroceryTableElement lowest;
		while (!groc.isEmpty()){
			lowest = getLowestInt(groc, option);
			sorted.add(lowest);
			groc.remove(lowest);
		}
		return sorted;
	}
	
	/**
	 * Helper function for sorting strings.
	 */
	
	private GroceryTableElement getLowestString(Vector<GroceryTableElement> groc, int option){
		
		if (option == 0){
			String lowest = groc.elementAt(0).getProductName();
			GroceryTableElement index = groc.elementAt(0);
			for (GroceryTableElement g : groc){
				if (lowest.compareTo(g.getProductName()) > 1) {
					lowest = g.getProductName();
					index = g;
				}
			}
			
			return index;
		}
		else {
			String lowest = groc.elementAt(0).getProductCategory();
			GroceryTableElement index = groc.elementAt(0);
			for (GroceryTableElement g : groc){
				if (lowest.compareTo(g.getProductCategory()) < 1) {
					lowest = g.getProductCategory();
					index = g;
				}
			}
			
			return index;
		}
	}
	
	/*
	 * Helper method for sorting number of purchases
	 */
	private GroceryTableElement getLowestInt(Vector<GroceryTableElement> groc, int option) {
		if (option == 0) {
			int lowest = groc.elementAt(0).getNumPurchases();
			GroceryTableElement index = groc.elementAt(0);
			for (GroceryTableElement g : groc){
				if (lowest < g.getNumPurchases()) {
					lowest = g.getNumPurchases();
					index = g;
				}
			}
			return index;
		}
		else {
			int lowest = groc.elementAt(0).getNumPurchases();
			GroceryTableElement index = groc.elementAt(0);
			for (GroceryTableElement g : groc){
				if (lowest > g.getNumPurchases()) {
					lowest = g.getNumPurchases();
					index = g;
				}
			}
			return index;
		}
	}
}


