/*
 * Creator: Caleb Keung
 */

package SelfCheckOut.Exceptions;

/**
 * An exception which is thrown when a BIC fails to construct
 * because the bulk item code provided is null or illegal. 
 */
public class InvalidEmployeeNumberException extends Exception {

	public InvalidEmployeeNumberException() {
		super();
	}

	public InvalidEmployeeNumberException(String message) {
		super(message);
	}

}
