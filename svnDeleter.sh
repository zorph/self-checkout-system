#! /bin/bash
function iterator {
	for file in `ls -a`
	do
		echo $file
		if [ -d $file ]
		then
			if [ $file == ".svn" ]
			then
				rm -rf $file
				echo "removed .svn folder"
			else
				echo "not svn"
				if [ $file != "." -a $file != ".." ]
				then
					cd $file
					iterator
				fi
			fi
		else
			echo "not a dir"
		fi
	done
}

iterator